package gophercises_algorithms

// Reverse will return the provided string in reverse order
// Reverse("cat") => "tac"
func Reverse(word string) string {
	var res string

	for _, w := range word {
		res = string(w) + res
	}

	return res
}
