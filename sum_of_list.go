package gophercises_algorithms

// Sum adds up all the numbers passed in and returns the result
func Sum(numbers []int) int {
	if len(numbers) == 0 {
		return 0
	}

	return numbers[0] + Sum(numbers[1:])
}
