package gophercises_algorithms

// NumberInList returns true is a number is in a list otherwise it
// returns false
func NumberInList(list []int, num int) bool {
	for _, n := range list {
		if n == num {
			return true
		}
	}
	return false
}
